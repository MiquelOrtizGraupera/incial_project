<h1>Barcelona</h1>

<p>Text amb informació de Barcelona, bonica ciutat del Mediterrani</p>

<h3>Llista ordenada de barris</h3>

<ol>
	<li>Nou barris</li>
	<li>Guinardo</li>
	<li>Trinitat</li>
</ol>


<h3>Llista desornada de barris</h3>

<ul>
	<li>Trinitat Nova</li>
	<li>Guineuta</li>
	<li>Ciutat Vella</li>

<strong>Enllaç a una web</strong>[Enllaç de Barcelona](https://www.barcelona.cat/ca/)


![imatge de Barcelona](../img/unnamed.jpg"Barcelona")
